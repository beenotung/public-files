# Public Share Files

## Why using GitLab?
I was using surge.sh to host the files previously, however, since surge.sh do full upload from client to server everytime I need to update (It then do partial update to the CDN). Which can take quite a long time when I'm under slow network.

## Future Direction
Host the files on IPFS, Surge.sh and GitLab. Then host the index on surge.

When storing the files on surge.sh, I've no choice but occupy a surge subdomain for each files to avoid full upload. The naming conversion in mind is to use the hash as subdomain name, and original file name as the file.

## Todo
 - write a script to host the index on surge.sh
